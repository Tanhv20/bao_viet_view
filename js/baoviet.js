

function showMenuMobile() {
  var x = document.getElementById("nav-register");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}
// $("#single_item").slick({
//   dots: true
// });
 
$(document).ready(function(){
    var stt=0;
    starImg = $("img.image:first").attr("stt");
    endImg  = $("img.image:last").attr("stt");
    $("img.image").each(function(){
        if($(this).is(':visible')){
            stt = $(this).attr("stt");
        }
    });
    $("#next").click(function(){
        if(stt == endImg){
            stt = starImg;
        }
        next = ++ stt;
        $("img.image.image").hide();
        $("img.image.image").eq(next).show();
    });
    $("#prev").click(function(){
        if(stt == starImg){
            stt = endImg;
            stt++;
        }
        prev = --stt;
        $("img.image.image").hide();
        $("img.image.image").eq(prev).show();
    });
    setInterval(function(){
        $("#next").click();
    },3000);
   

});